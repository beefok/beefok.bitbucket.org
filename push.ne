@{% function biop(d) { return {type: "biop", op: d[2], a: d[0], b: d[4]}; }
    function unop(d) {
		if (d[2].v)
		return {type: "unop", op: d[0], a: d[2], b: null};
	}
    function num (d) {
    var b = 10; if (d.length == 2) { d = d[0]+d[1].join(""); } else { d = d[0]; }
	if (d.length >= 2) { if (d[0] == "0") { if (d[1] == "x") { b = 16; } else { b = 8; } } }
	return {type: "num", v: parseInt(d, b) };
	}
 %}

main     -> _ main _ NL _ stmt         {% function(d) { return {type: "block", a: d[0], b:d[4]}; } %}
         |  _ stmt                     {% function(d) { return {type: "block", a: d[1], b:null}; } %}
     
args     -> args _ "," _ expr          {% function(d) { return [d[0], d[4]]; } %}
         |  expr                       {% id %}
     
atom     -> "(" _ expr _ ")"           {% function(d) { return d[2]; } %}
         |  TXT _ "(" _ args _ ")"     {% function(d) { return {type: "call", name: d[0].v, args: d[4]}; } %}
         |  TXT _ "(" _ ")"            {% function(d) { return {type: "call", name: d[0].v, args: null}; } %}
         |  TXT                        {% id %}
         |  NUM                        {% id %}
		 
exp5     -> "-" _ atom                 {% function(d) { return unop(d); } %}
         |  "+" _ atom                 {% function(d) { return unop(d); } %}
         |  "!" _ atom                 {% function(d) { return unop(d); } %}
         |  "~" _ atom                 {% function(d) { return unop(d); } %}
         | atom                        {% id %}
		 
exp4     -> exp4 _ "&&" _ exp5         {% function(d) { return biop(d); } %}
         |  exp4 _ "||" _ exp5         {% function(d) { return biop(d); } %}
         |  exp5                       {% id %}
		 
exp3     -> exp3 _ "<=" _ exp4         {% function(d) { return biop(d); } %}
         |  exp3 _ ">=" _ exp4         {% function(d) { return biop(d); } %}
         |  exp3 _ "<"  _ exp4         {% function(d) { return biop(d); } %}
         |  exp3 _ ">"  _ exp4         {% function(d) { return biop(d); } %}
         |  exp3 _ "==" _ exp4         {% function(d) { return biop(d); } %}
         |  exp3 _ "!=" _ exp4         {% function(d) { return biop(d); } %}
         |  exp4                       {% id %}
		 
exp2     -> exp2 _ "<<" _ exp3         {% function(d) { return biop(d); } %}
         |  exp2 _ ">>" _ exp3         {% function(d) { return biop(d); } %}
         |  exp2 _ "&"  _ exp3         {% function(d) { return biop(d); } %}
         |  exp2 _ "|"  _ exp3         {% function(d) { return biop(d); } %}
         |  exp2 _ "^"  _ exp3         {% function(d) { return biop(d); } %}
         |  exp3                       {% id %}
		 
exp1     -> exp1 _ "*" _ exp2          {% function(d) { return biop(d); } %}
         |  exp1 _ "/" _ exp2          {% function(d) { return biop(d); } %}
		 |  exp1 _ "%" _ exp2          {% function(d) { return biop(d); } %}
		 |  exp2                       {% id %}
     
exp0     -> exp0 _ "+" _ exp1          {% function(d) { return biop(d); } %}
         |  exp0 _ "-" _ exp1          {% function(d) { return biop(d); } %}
         |  exp1                       {% id %}
		 
expr     -> exp0                       {% id %}
     
stmt     -> TXT _ "=" _ expr           {% function(d) { return biop(d); } %}
		 |  TXT __ args                {% function(d) { return {type: "call", name: d[0].v, args: d[2]}; } %}
         |  TXT                        {% function(d) { return {type: "call", name: d[0].v, args: null}; } %}

NUM      ->  [0-9] [_xa-fXA-F0-9]:*    {% function(d) { return num(d); } %}
TXT      ->  [_a-z] [_a-zA-Z0-9]:*     {% function(d) { return {type: "txt", v: d[0]+d[1].join("")}; } %}

# Whitespace: `_` is optional, `__` is mandatory.
_        -> wschar:*                   {% id %}
__       -> wschar:+                   {% id %}
wschar   -> [ \t\v\f]                  {% id %}
NL       -> [\n]:+                     {% id %}
