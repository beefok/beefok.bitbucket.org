
function toHex(num, len) {
   txt = num.toString(16);
   while (txt.length < len) {
      txt = "0" + txt;
   }
   return "$" + txt;
}
   
function simulator() {
   var p, t, n, s, v, x, y, g, i;
   var temp, ptr;
   
   // data memory is 4096 x 16 ( 8 KB)
   var data = new Array(0x1000);
   
   // main memory is 65536 x 8 (64 KB)
   var mem  = new Array(0x10000);
   
   // i/o bus (for now it's just a single register)
   var bus;
   
   function wrP(word) {
      if (word > 0xFFFF) {
         // code overflow
      }
      else if (word < 0) {
         // code underflow
      }
      p = word & 0xFFFF;
   }
   
   function wrT(word) { t = word & 0xFFFF; }
   function wrN(word) { n = word & 0xFFFF; }
   function wrS(word) { s = word & 0xFFF;  }
   function wrV(word) { v = word & 0xFFF;  }
   function wrX(word) { x = word & 0xFFF;  }
   function wrY(word) { y = word & 0xFFF;  }
   function wrG(word) { g = word & 0xFFF;  }
   function wrI(word) { i = word & 0xFFFF; }
   
   function rdMem(addr) {
      return ((mem[addr & 0xFFFF] & 0xFF) << 8) | (mem[(addr + 1) & 0xFFFF] & 0xFF);
   }
   
   function wrMem(addr, word) {
      mem[addr & 0xFFFF] = word & 0xFF;
      mem[(addr+1) & 0xFFFF] = (word >> 8) & 0xFF;
   }
   
   function rdBus(addr) {
      // todo: actually make this work
      return bus;
   }
   
   function wrBus(addr, word) {
      // todo: actually make this work
      // do nothing for now
      bus = word;
   }
   
   function rdData(addr) {
      if ((addr > 0xFFF) || (addr < 0)) {
         // error: data underflow/overflow
      }
      return data[addr & 0xFFF] & 0xFFFF;
   }
   
   function wrData(addr, word) {
      if ((addr > 0xFFF) || (addr < 0)) {
         // error: data underflow/overflow
      }
      data[addr & 0xFFF] = word & 0xFFFF;
   }
   
   function popStack(updateT = true) {
      wrS(s+1);
      if (updateT == true) {
         wrT(n);
      }
      wrN(rdData(s));
   }
   
   function pshStack(updateN = true) {
      wrData(s, n);
      if (updateN == true) {
         wrN(t);
      }
      wrS(s-1);
   }
   
   function u12(word) {
      return word & 0xFFF;
   }
   
   function s12(word) {
      // negative version
      if ((word & 0xFFF) >= 0x800) {
         return ((word & 0x7FF) - 0x800) & 0xFFFF;
      }
      return u12(word);
   }
   
   function s7(word) {
      temp = ((word >> 8) & 0x7F);
      // negative version
      if (temp > 0x40) {
         return ((temp & 0x3F) - 0x40) & 0xFFFF;
      }
      return temp;
   }
   
   function varg(word) {
      // 8-bit varg
      if (((word >> 8) & 0xF) == 0) {
         return (word & 0xFF) + 0x10;
      }
      return (word >> 8) & 0xF;
   }
   
   function reset() {
      ptr = 0;
      wrP(0);
      wrT(0);
      wrN(0);
      wrS(-1);
      wrV(-1);
      wrX(0);
      wrY(0);
      wrG(0);
      wrI(0x3000); // start out with a reset instruction
      bus = 0xABCD;
   }
   
   var size = 1;
   
   var instruction = {
      // j 12-bit signed (jump pc relative)
      0: function (word, act = true) {
         if ((word & 0x0FFF) == 0x0000) {
            if (act == true) {
               wrP(p-1);
            }
            return {name: "brk", arg: "", size: 2};
         }
         arg = s12(word);
         
         if (act == true) {
            wrP(p+arg);
         }
         return {name: "j", arg: arg, size: 2};
      },
      
      // jz 12-bit signed (jump pc relative if t = 0)
      1: function (word, act = true) {
         arg = s12(word);
         
         if ((word & 0x0FFF) == 0x0000) {
            if (act == true) {
               wrP(p-1);
            }
            return {name: "brk", arg: "", size: 2};
         }
         
         if (act == true) {
            if (t == 0) {
               tmp = p+arg;
               name = "jz:1";
            }
            else {
               tmp = p+1;
               name = "jz:0";
            }
            wrP(tmp);
            popStack();
         }
         else {
            name = "jz";
         }
         
         return {name: name, arg: arg, size: 2};
      },
      
      // read from i/o bus, load global
      2: function (word, act = true) {
         // read from i/o bus
         arg = u12(word);
         
         //todo: remove?
         //if (act == true) {
         //   pshStack();
         //}
         
         if (arg >= 0xF00) {
            arg &= 0xFF;
            if (act == true) {
               tmp = rdBus(arg);
            }
            name = "bi";
         }
         // load global
         else {
            if (act == true) {
               tmp = rdData(g + arg);
            }
            name = "lg";
         }
         
         if (act == true) {
            pshStack();
            wrT(tmp);
            wrP(p+1);
         }
         return {name: name, arg: arg, size: 2};
      },
      
      // reset, interrupt, write to i/o bus, store global
      3: function (word, act = true) {
         arg = u12(word);
         // reset
         if (arg == 0x000) {
            if (act == true) {
               reset();
            }
            name = "reset";
         }
         // interrupt
         else if (arg < 0x010) {
            if (act == true) {
               pshStack();
               wrT(p+1);
               wrP(arg << 1 - 1);
            }
            name = "irq";
         }
         // write to i/o bus
         else if (arg >= 0xF00) {
            arg &= 0xFF;
            if (act == true) {
               wrBus(arg, t);
               popStack();
            }
            name = "bo";
         }
         // store global
         else {
            if (act == true) {
               wrData(g + arg, t);
               popStack();
            }
            name = "sg";
         }
         if (act == true) {
            wrP(p+1);
         }
         return {name: name, arg: arg, size: 2};
      },
      
      // load variable
      4: function (word, act = true) {
         arg = varg(word);
         
         if (arg >= 0x10) {
            size = 2;
         }
         else {
            size = 1;
         }
         
         if (act == true) {
            pshStack();
            wrT(rdData(v+arg));
            // 8-bit varg
            if (size == 2) {
               wrP(p+1);
            }
         }
         
         return {name: "lv", arg, size: size};
      },
      
      // store variable
      5: function (word, act = true) {
         arg = varg(word);
         
         if (arg >= 0x10) {
            size = 2;
         }
         else {
            size = 1;
         }
         
         if (act == true) {
            wrData(v+arg, t);
            popStack();
            // 8-bit varg
            if (size == 2) {
               wrP(p+1);
            }
         }
         return {name: "sv", arg, size: size};
      },
      
      // alu operation
      6: function (word, act = true) {
         name = "error";
         arg = "";
         op = (word >> 8) & 0xF;
         
         switch (op) {
         // nop
         case 0x0:
            name = "nop";
            break;
            
         // ash
         case 0x1:
            arg = t & 0x1F;
            if (t >= 0xFFEF) {
               name = "asr";
               temp = (n >> (-arg)) | ((n >= 0x8000) ? (0xFFFF << (arg & 0xF)) : 0x0000);
            }
            else {
               name = "asl";
               temp = (n << ( arg)) | (((n & 1) == 1) ? (0xFFFF >> (16-arg)) : 0x0000);
            }
            if (act == true) {
               popStack();
               wrT(temp);
            }
            break;
            
         // lsh
         case 0x2:
            arg = t & 0x1F;
            if (t >= 0xFFF0) {
               name = "lsr";
               temp = (n >> (-arg));
            }
            else {
               name = "lsl";
               temp = (n << arg);
            }
            if (act == true) {
               popStack();
               wrT(temp);
            }
            break;
         
         // rot
         case 0x3:
            arg = t & 0x1F;
            if (t >= 0xFFF0) {
               name = "ror";
               temp = (n >> (-arg)) | (n << (arg & 0xF));
            }
            else {
               name = "rol";
               temp = ((n << arg) | (n >> (16-arg)));
            }
            if (act == true) {
               popStack();
               wrT(temp);
            }
            break;
         
         // inc
         case 0x4:
            if (act == true) {
               wrT(t+1);
            }
            name = "inc";
            break;
            
         // dec
         case 0x5:
            if (act == true) {
               wrT(t-1);
            }
            name = "dec";
            break;
            
         // lr
         case 0x6:
            if (act == true) {
               wrT(n<t ? 1:0);
               popStack(false);
            }
            name = "lr";
            break;
            
         // gr
         case 0x7:
            if (act == true) {
               wrT(n>t ? 1:0);
               popStack(false);
            }
            name = "gr";
            break;
            
         // adc
         case 0x8:
            if (act == true) {
               temp = (t+n) & 0x1FFFF;
               wrT((temp >> 16) & 0x1);
               wrN(temp & 0xFFFF);
            }
            name = "adc";
            break;
            
         // sbc
         case 0x9:
            if (act == true) {
               temp = (t-n) & 0x1FFFF;
               wrT((temp >> 16) & 0x1);
               wrN(temp & 0xFFFF);
            }
            name = "sbc";
            break;
         
         // add
         case 0xA:
            if (act == true) {
               wrT(t+n);
               popStack(false);
            }
            name = "add";
            break;
            
         // sub
         case 0xB:
            if (act == true) {
               wrT(t-n);
               popStack(false);
            }
            name = "sub";
            break;
            
         // nor
         case 0xC:
            if (act == true) {
               wrT(~(t|n));
               popStack(false);
            }
            name = "nor";
            break;
            
         // and
         case 0xD:
            if (act == true) {
               wrT(t&n);
               popStack(false);
            }
            name = "and";
            break;
         
         // or
         case 0xE:
            if (act == true) {
               wrT(t|n);
               popStack(false);
            }
            name = "or";
            break;
         
         // xor
         case 0xF:
            if (act == true) {
               wrT(t^n);
               popStack(false);
            }
            name = "xor";
            break;
         }
         
         return {name: name, arg: "", size: 1};
      },
      
      // system operation
      7: function (word, act = true) {
         name = "error";
         op = (word >> 8) & 0xF;
         
         switch (op) {
         // dup
         case 0x0:
            if (act == true) {
               pshStack();
               wrN(t);
            }
            name = "dup";
            break;
            
         // swap
         case 0x1:
            if (act == true) {
               temp = t;
               wrT(n);
               wrN(temp);
            }
            name = "swap";
            break;
            
         // drop
         case 0x2:
            if (act == true) {
               popStack();
            }
            name = "drop";
            break;
            
         // over
         case 0x3:
            if (act == true) {
               temp = n;
               pshStack();
               wrT(temp);
            }
            name = "over";
            break;
         
         // call
         case 0x4:
            if (act == true) {
               temp = t;
               wrT(p);
               wrP(temp);
            }
            name = "call";
            break;
         
         // goto
         case 0x5:
            if (act == true) {
               wrP(t);
               popStack();
            }
            name = "goto";
            break;
         
         // enter
         case 0x6:
            if (act == true) {
               pshStack();
               wrV(s-t-1);
               wrS(s-t-1);
               wrT(v);
            }
            name = "enter";
            break;
         
         // exit
         case 0x7:
            if (act == true) {
               wrS(v);
               wrV(t);
               popStack();
            }
            name = "exit";
            break;
         
         // lx
         case 0x8:
            if (act == true) {
               pshStack(false);
               wrN(rdData(t+x));
            }
            name = "lx";
            break;
            
         // sx
         case 0x9:
            if (act == true) {
               wrData(t+x, n);
               popStack(false);
            }
            name = "sx";
            break;
            
         // ix
         case 0xA:
            if (act == true) {
               pshStack();
               wrT(x);
            }
            name = "ix";
            break;
         
         // ox
         case 0xB:
            if (act == true) {
               wrX(t);
               popStack();
            }
            name = "ox";
            break;
         
         // ly
         case 0xC:
            if (act == true) {
               pshStack(false);
               wrN(rdData(t+y));
            }
            name = "ly";
            break;
         
         // sy
         case 0xD:
            if (act == true) {
               wrData(t+y, n);
               popStack(false);
            }
            name = "sy";
            break;
         
         // iy
         case 0xE:
            if (act == true) {
               pshStack();
               wrT(y);
            }
            name = "iy";
            break;
         
         // oy
         case 0xF:
            if (act == true) {
               wrY(t);
               popStack();
            }
            name = "oy";
            break;
         }
         
         return {name: name, arg: "", size: 1};
      },
      
      // immediate instruction (im $00-7F)
      8: function (word, act = true) {
         arg = s7(word);
         if (act == true) {
            pshStack();
            wrT(arg);
         }
         return {name: "im", arg: arg, size: 1};
      }
   };
   
   function orgMem(word) {
      ptr = word & 0xFFFF;
   }
   
   function pshMem(word) {
      wrMem(ptr, word & 0xFF);
      ptr++;
   }
   
   function getOp(word, act = true) {
      var txt, op;
      
      // 4-bit instruction opcode
      op = (word >> 12) & 0xF;
      
      // display address and data
      txt = "";
      
      // grab immediate
      if (op > 0x8) {
         op = 0x8;
      }
      
      // execute instruction (or decode)
      ranOp = instruction[op](word, act);
      
      // increase length of name text to keep space proper
      while (ranOp.name.length < 5) { ranOp.name += " "; }
      
      // include opcode
      txt += ranOp.name;
      
      // include argument if there is one
      if (!(ranOp.arg === "")) {
         txt += " " + toHex(ranOp.arg, 4);
      }
      
      return {name:txt, size:ranOp.size};
   }
   
   function tick() {
      // grab the code location
      i = rdMem(p);
      
      // increment p
      wrP(p+1);
      
      // now run the instruction
      return getOp(i, true);
   }
   
   function rdRegs() {
      return {
         p: p,
         t: t,
         n: n,
         s: s,
         v: v,
         x: x,
         y: y,
         g: g
      };
   }
   
   function rdStack() {
      var txt = "";
      
      txt += " ...t:" + toHex(t,4) + "\n ...n:" + toHex(n,4) + "\n ";
      for (i = s+1; i <= 0xFFF; i++) {
         txt += toHex(i,3) + ":" + toHex(rdData(i),4) + "\n ";
      }
      return txt;
   }
   
   return {
      getOp:   getOp,
      reset:   reset,
      tick:    tick,
      pshMem:  pshMem,
      orgMem:  orgMem,
      rdMem:   rdMem,
      wrMem:   wrMem,
      rdData:  rdData,
      wrData:  wrData,
      rdRegs:  rdRegs,
      rdStack: rdStack
   };
}